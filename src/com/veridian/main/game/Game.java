package com.veridian.main.game;

import org.lwjgl.opengl.GL11;


import com.veridian.main.game.level.Level;




public class Game {
	
	Level level;
	
	public static float xScroll, yScroll;
	
	public Game(){
		level= new Level(64,64);
		xScroll = level.getBounds(0);
		yScroll = level.getBounds(1);
	}
	
	public void init(){
		level.init();
	}
	
	public void translateView(float xa, float ya){
		
		if(xScroll>level.getBounds(0) || 
				xScroll<level.getBounds(2)|| 
				yScroll>level.getBounds(1) || 
				yScroll<level.getBounds(3)				
					){
				return;
			}
		
		
		xScroll+=xa;
		yScroll+=ya;
	}
	
	float xa=1;
	float ya=1;
	public void update(){
		if(xScroll==level.getBounds(0) || xScroll==level.getBounds(2)){
			xa =-xa;
		}
		if(yScroll==level.getBounds(1) || yScroll==level.getBounds(3)){
			ya =-ya;
		}
		translateView(xa,ya);
		level.update();
	}
	
	public void render(){
		GL11.glTranslatef(xScroll, yScroll,0);
		level.render();
	}
}
